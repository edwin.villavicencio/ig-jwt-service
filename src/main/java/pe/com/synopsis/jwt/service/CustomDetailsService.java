package pe.com.synopsis.jwt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import pe.com.synopsis.jwt.beans.domain.CustomUser;
import pe.com.synopsis.jwt.beans.domain.UserEntity;
import pe.com.synopsis.jwt.repositories.OAuthDao;

@Service
public class CustomDetailsService implements UserDetailsService {

	@Autowired
	OAuthDao oAuthDao;
	
	@Override
	public CustomUser loadUserByUsername(final String userName) throws UsernameNotFoundException {
		UserEntity userEntity = null;
		
		try {
			userEntity = oAuthDao.getUserDetails(userName);
			CustomUser customUser = new CustomUser(userEntity);
			return customUser;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new UsernameNotFoundException("User " + userName + "no fue encontrada en la base de datos");
		}
	}
}
