package pe.com.synopsis.jwt.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
public class OAuth2Config extends AuthorizationServerConfigurerAdapter{

	private String clientId = "tutorialspoint";
	private String clientSecret = "my-secret-key";
	private String privateKey = "MIIEowIBAAKCAQEArd86m+m4hXPLB4MR8FzTyGhRIMLx27o3vVNRZK+jb7MPYK4Q" + 
			"OdkWh6OEBSoK07v4QOoO/M1BZxi1Ohcr+noXLeawtk6XFSUH9jkCyybalcW2sQvW" + 
			"bKJLqHSoq4PhI2wpvhLQVDW2sua3aU2EHs9DgyG6ZePNBSPiyNL6nRKFvZLxT87Q" + 
			"ssRYCoWP/XemBn8qTABQ/x+EpHRSgW1mo2T4Ts5jycMC/a+wRyn58m7vrQ6Cl8Hv" + 
			"Qo66iuQLemZ7QI3YzXEyIQriMc+LF3gyGaktVSFdPQb2QYVRfEgC08oG25jZM7TD" + 
			"cgZJJWVYMol7yTkILVTGtkrxpHMDFbOwI5yfEQIDAQABAoIBAQCVKAPiTqzhpwUL" + 
			"66/O8AMqT/sQzMuogGZivv/7DQV6xDQz7v6ycX33XBncSEXDf+Qm4+N5kSdDJxoI" + 
			"vGoY1PXfhaAgtOi81eivnBa+ahVoGJVx6kLHLK8U0wcDI/5WYeqpSQB4zR9u96dt" + 
			"6Xam64zyCFQcJkDx4Ogi+YLOjl420lZVziqrKk4zzXfXQQQl4QXCZVE0dG/o5ATP" + 
			"fUgEW7rqFlf258ZD8VB4ADRSTSnt8noFQuHhH1DCJ40dMWNnsqGDe9YYMp4HpeTu" + 
			"A5sP/saj9XAegicokjkFoeiL5IzW317/lL8/VMaONOnI0wqk91+iVd+E7C431Ove" + 
			"zkP9TJVlAoGBAN1WKYGS1grKY0yTR61gtjrZp8/GbBosFZdAKVV/Yqnn1D8CSkv0" + 
			"B645urGPApCeYgreTqFKywAtc2mGI8HDZ1vKtqRdDbAMrgNKwh66nQ/gHRJOW3zy" + 
			"Yof7NmqdAi2pGvbvxpK9WIkzKhSAfbdftRC++epXOGf8OapM0eoFdQh7AoGBAMka" + 
			"HToQP7Yck89cr+FyvWcv4zyf6myw16U2wY7XLNNBOk+Oy7udf/jd4T7JkaBtH86S" + 
			"37sa6ELMUwsEaygUDULd9Txx+TS9Qys1xU+qWmitSJN1xQsFZBE+nH89OlirXMIN" + 
			"7XU6/Me26VWHLQqx3FsEO/iH6MEp6jyDdZNW8C7jAoGASWX6S6rK6jclaM5qdZdd" + 
			"tvRYzvEpBiDaZBb13hFpQZjMbLywsLFTHrasL5eSLS0orBv+jURFP2DTJgpCr03C" + 
			"Z9E4chcdo+UAi7zt1MzjqtESMvDjK/MhM6Dd8crxXvz2IS9wdVbvFkhuSvA1QGsv" + 
			"m0cJj2mvHeSppszky6tVnSMCgYASyRe11tK8xkqgcj4Tx04eF4EYpq+VW6epTUYj" + 
			"e8I6MbuqtuIdModDziY+YUHzRH5WI642ojrUEpYnT1BbPBRJDMbd0J/A53Ug0jMO" + 
			"UBGF+bnv3G59qxE/8yNXV3bd1ZQipH5sFwiJ93IwhSIV/6tf7TNU8g0yDiJNaNJQ" + 
			"4ywolQKBgADq0tsilKM3vw5+VvvNTE+thf3j3KjRj2sxrCllDiW1LSxfybP/SMvf" + 
			"N5hC+oDA78fV23SO1XV/FLc4zG5W2zFLlhjzTRiH5THf7zChN+xKkxYOgEIgPOCR" + 
			"sCpDJzyutBlWMn5HK/zKMc01SVpSlO5Q+L/L1zw/nl7O0o0mSego";
	private String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArd86m+m4hXPLB4MR8FzT" + 
			"yGhRIMLx27o3vVNRZK+jb7MPYK4QOdkWh6OEBSoK07v4QOoO/M1BZxi1Ohcr+noX" + 
			"Leawtk6XFSUH9jkCyybalcW2sQvWbKJLqHSoq4PhI2wpvhLQVDW2sua3aU2EHs9D" + 
			"gyG6ZePNBSPiyNL6nRKFvZLxT87QssRYCoWP/XemBn8qTABQ/x+EpHRSgW1mo2T4" + 
			"Ts5jycMC/a+wRyn58m7vrQ6Cl8HvQo66iuQLemZ7QI3YzXEyIQriMc+LF3gyGakt" + 
			"VSFdPQb2QYVRfEgC08oG25jZM7TDcgZJJWVYMol7yTkILVTGtkrxpHMDFbOwI5yf" + 
			"EQIDAQAB";
	
	@Autowired
	@Qualifier("authenticationManagerBean")
	private AuthenticationManager authenticationManager;
	
	@Bean
	public JwtAccessTokenConverter tokenEnhancer() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		converter.setSigningKey(privateKey);
		converter.setVerifierKey(publicKey);
		return converter;
	}
	
	@Bean
	public JwtTokenStore tokenStore() {
		return new JwtTokenStore(tokenEnhancer());
	}
	
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endPoints) throws Exception {
		endPoints.authenticationManager(authenticationManager).tokenStore(tokenStore())
			.accessTokenConverter(tokenEnhancer());
	}
	
	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
	}
	
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory().withClient(clientId).secret(clientSecret).scopes("read", "write")
			.authorizedGrantTypes("password", "refresh_token").accessTokenValiditySeconds(20000)
			.refreshTokenValiditySeconds(20000);
	}
}
