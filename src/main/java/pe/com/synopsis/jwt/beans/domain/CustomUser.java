package pe.com.synopsis.jwt.beans.domain;

import org.springframework.security.core.userdetails.User;

public class CustomUser extends User {

	private static final long serialVersionUID = 5983632065234260284L;

	public CustomUser(UserEntity user) {
		super(user.getUserName(), user.getPassword(), user.getGrantedAuthoritiesList());
	}
}
